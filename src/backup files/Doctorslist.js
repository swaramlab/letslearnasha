// import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
// import axios from 'axios';
import React,{Component} from 'react';
import './styles.css'
// import AppBar from 'material-ui/AppBar';
// import RaisedButton from 'material-ui/RaisedButton';
// import TextField from 'material-ui/TextField';
//import service from '../server/Doctorlist.json';
import DataGrid, { Column } from 'devextreme-react/data-grid';
class Doctorslist extends Component {
  constructor(props) {
    super(props);
    this.employees = service.getEmployees();
  }

  render() {
    return (
      <DataGrid id="gridContainer"
        dataSource={this.employees}
        showBorders={true}
      >
        <Column dataField="Doctor" />
        <Column dataField="Appt Date" dataType="date" />
        <Column dataField="Duration" />
        <Column dataField="Amount" />
        <Column dataField="Status" />
        <Column dataField="join" dataType="button" />
        <Column dataField="cancel" dataType="button" />
      </DataGrid>
    );
  }
}

function cellRender(data) {
  return <img src={data.value} />;
}

export default Doctorslist;