import React, { Component } from "react";
import {Route,NavLink,HashRouter} from 'react-router-dom';
import Login from './Login';
import './index.css';
 
class Main extends Component {
  render() {
    return (
    <HashRouter>
       
        <div>
         <ul className="header">
            <img src="logo.png" alt="Logo"/>
            <li><NavLink to= "/Login">Login</NavLink></li>
            <li><NavLink to= "/#">Doctor</NavLink></li>
            <li><NavLink to= "/#">Patient</NavLink></li>
            <li><NavLink to= "/#">Pages</NavLink></li>
            <li><NavLink to= "/#">Blog</NavLink></li>
            <li><NavLink to= "/#">Admin</NavLink></li>
        </ul>
        </div>
        <div>
          
          <ul className="headers">
            
          </ul>
          <div className="content">
            
            <Route path="/Login" component={Login}/>
            {/* {/* {/* //<Route path="/Doctorslist" component={Doctorslist}/>  */}
             
          </div>
        </div>
</HashRouter>
    );
  }
}
 
export default Main;