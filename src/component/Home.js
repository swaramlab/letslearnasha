import React, { Component } from "react";
 
class Home extends Component {
  render() {
    return (
      <div>
        <h2>HELLO</h2>
        <p>Our Vision is to heal and comfort the suffering humanity with compassion and respect, and to be recognized as a global leader in medical education and research. </p>
 
        <p>To be progressive in providing holistic health care services to all.
            To ensure global standards in medical education.
            To create and foster centre of excellence for medical research.</p>
      </div>
    );
  }
}
 
export default Home;
