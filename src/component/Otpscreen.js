import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import React,{Component} from 'react';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
class Otpscreen extends Component {
constructor(props){
  super(props);
  this.state={
    phone:'',
    otp:''
  }}
  LoginHandler=(event)=>{
    var apiBaseUrl = "https://api.letslrn.net/auth/validate";
    
    var payload={
    token:this.state.token
     }
    axios.post(apiBaseUrl, payload)
    .then((response)=> {
    console.log(response);
    if(response.data.code === 200){
    console.log("Login successfull");
    }
    else if(response.data.code === 404){
    console.log("wrong otp");
    alert("Pleas check the OTP")
    }
    })
    .catch( (error)=> {
    console.log(error);
    })
    }
render() {
    return (
      <div>
        <MuiThemeProvider>
          <div>
          <AppBar
             title="Otpscreen"
           />
           <TextField
             hintText="Please enter OTP"
             floatingLabelText="OTP"
             onChange = {(event,newValue) => this.setState({mobileno:newValue})}
             />
           <br/>
            <br/>
             <RaisedButton label="Login" primary={true} style={style} onClick={(event) => this.LoginHandler(event)}/>
         </div>
         </MuiThemeProvider>
      </div>
    );
  }
}
const style = {
 margin: 15,
};
export default Otpscreen;