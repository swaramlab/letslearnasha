import React from 'react';
import ReactDOM from 'react-dom';
import Main from './component/Main';
import './index.css';
//import App from './App';//<App />
//import Doctorslist from './component/Doctorslist';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
      <Main/>,
  // <React.StrictMode>
  //   </React.StrictMode>,
  document.getElementById('root'),
  
  

);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
